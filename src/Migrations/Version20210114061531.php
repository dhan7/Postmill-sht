<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20210114061531 extends AbstractMigration
{
    public function getDescription(): string {
        return 'Add site settings for external links open new tab';
    }

    public function up(Schema $schema): void {
        $this->addSql('ALTER TABLE sites ADD open_external_links_in_new_tab BOOLEAN DEFAULT TRUE NOT NULL');
        $this->addSql('UPDATE users SET open_external_links_in_new_tab = TRUE');
    }

    public function down(Schema $schema): void {
        $this->addSql('ALTER TABLE sites DROP open_external_links_in_new_tab');
    }
}
